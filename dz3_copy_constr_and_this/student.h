#include<iostream>
#include <windows.h>
using namespace std;


class Student
{
public:
	Student(char *name, char * surname, char * midllname, char * bridhday, char * adress,char * phone, char * faculty, int course);
	Student();
	Student(Student & obj);
	~Student();
	void setName(char *);
	void setSurname(char *);
	void setMidllname(char *);
	void setBridhday(char*);
	void setAdress(char *);
	void setPhone(char *);
	void setFaculty(char *);
	void setCourse(int);
	char * getName();
	char * getSurname();
	char * getMidllname();
	char * getBridhday();
	char * getAdress();
	char * getPhone();
	char * getFaculty();
	int getCourse();
	int getYear();
	static void getAllStudents();
	static void getFacultiyStudents(char *);
	static void getYearStudents(int);
	static void getFacultiyAndCourceStudents();
private:
	struct data {
		int day;
		int month;
		int year;
	};

	char name[30];
	char surname[30];
	char midllname[30];
	data bridhday;
	char returnData[30];
	char adress[100];
	char phone[20];
	char faculty[30];
	int course;
	int uuid;
	static int globalCount;
	static Student ** arrayStudents;
	static Student ** addNewStudent(Student *);
	static Student ** deleteStudent(Student *);
	static int compare(const void * a, const void * b);
};







